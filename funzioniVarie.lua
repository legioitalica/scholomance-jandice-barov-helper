-- Author      : Galilman
-- Create Date : 21/08/2014 16:53:11

local ScholomanceJandiceBarovHelper = _G.ScholomanceJandiceBarovHelper

local L = LibStub("AceLocale-3.0"):GetLocale("ScholomanceJandiceBarovHelper")

nuovaVersione = false
nuovaVersioneStr = "0.0.0"
richiestaMia = false

versioneCorrente = tostring (GetAddOnMetadata ("ScholomanceJandiceBarovHelper", "Version")) or "0.0.0"

local tabellaMark = {
	"{Star}",
	"{Circle}",
	"{Diamond}",
	"{Triangle}",
	"{Moon}",
	"{Square}",
	"{Cross}",
	"{Skull}"
}

function ScholomanceJandiceBarovHelper:targetInfo () 
	ret = {}
	
	ret["nome"] = UnitName ("target") or ""
	ret["GUID"] = UnitGUID ("target") or ""

	if ret["GUID"] ~= nil and ret["GUID"] ~= "" then
		local B = tonumber(ret["GUID"]:sub(5,5), 16) or 0xff
		local maskedB = B % 8 -- x % 8 has the same effect as x & 0x7 on numbers <= 0xf
		local knownTypes = {
			[0] = "player", 
			[3] = "NPC", 
			[4] = "pet", 
			[5] = "vehicle"
		}

		ret["tipo"] = knownTypes[maskedB] or "sconosciuto"
		ret["id"] = tonumber(ret["GUID"]:sub(-12, -9), 16)
	else
		ret["GUID"] = ""
		ret["tipo"] = "sconosciuto"
		ret["id"] = -1
	end

	local morto = UnitIsDead ("target")
	ret["stato"] = morto and "morto" or "vivo"

	return ret
end

function ScholomanceJandiceBarovHelper:mouseoverInfo () 
	ret = {}
	
	ret["nome"] = UnitName ("mouseover") or ""
	ret["GUID"] = UnitGUID ("mouseover") or ""

	if ret["GUID"] ~= nil and ret["GUID"] ~= "" then
		local B = tonumber(ret["GUID"]:sub(5,5), 16) or 0xff
		local maskedB = B % 8 -- x % 8 has the same effect as x & 0x7 on numbers <= 0xf
		local knownTypes = {
			[0] = "player", 
			[3] = "NPC", 
			[4] = "pet", 
			[5] = "vehicle"
		}

		ret["tipo"] = knownTypes[maskedB] or "sconosciuto"
		ret["id"] = tonumber(ret["GUID"]:sub(-12, -9), 16)
	else
		ret["GUID"] = ""
		ret["tipo"] = "sconosciuto"
		ret["id"] = -1
	end

	local morto = UnitIsDead ("mouseover")
	ret["stato"] = morto and "morto" or "vivo"

	return ret
end

function ScholomanceJandiceBarovHelper:IsDebug ()
	return self.db.profile.debugAttivo
end

function ScholomanceJandiceBarovHelper:SetDebug (dbg)
	self.db.profile.debugAttivo = dbg

	self:Print (L["msg_debug"] .. (self:IsDebug() and L["debug_abilitato"] or L["debug_disabilitato"]))
end

function ScholomanceJandiceBarovHelper:IsModelliCollegati ()
	return self.db.profile.modelliCollegati
end

function ScholomanceJandiceBarovHelper:SetModelliCollegati (dbg)
	self.db.profile.modelliCollegati = dbg
end

function ScholomanceJandiceBarovHelper:split(str, sep)
	if sep == nil then
		sep = "%s"
	end
    
    local t={} 
	local i=1

	for str in string.gmatch(str, "([^" .. sep .. "]+)") do
		t[i] = str
        i = i + 1
    end

    return t
end

function ScholomanceJandiceBarovHelper:setMark (index)
	SetRaidTargetIcon ("target", index)

	local mark = tabellaMark[index] or "bho"
	local messaggio = string.format (L["copia_trovata"], mark)

	local canale = nil
	local playerName = nil
	--Controllo se � in lfg
	if IsInGroup(LE_PARTY_CATEGORY_INSTANCE) and IsInInstance() then
		--Se lfg manda il messaggio in instance chat
		canale = "INSTANCE_CHAT"
	elseif IsInRaid() then -- Controllo se raid group
		if UnitIsGroupAssistant ("player") then -- In caso sia raid group controllo se il player ha assist
			canale = "RAID_WARNING" -- Mando il messaggio in raid warning
		else
			canale = "RAID" -- Mando il messaggio in raid
		end
	elseif IsInGroup(LE_PARTY_CATEGORY_HOME) then -- Controllo se party
		canale = "PARTY" -- Mando in party
	else
		canale = "WHISPER" -- In caso non sia in nessun tipo di gruppo mando un whisper al player
		playerName = GetUnitName("player")
	end
	SendChatMessage (messaggio, canale, nil, playerName)
	SendChatMessage (messaggio, "YELL")
end

function ScholomanceJandiceBarovHelper:stampaInfo ()
	local index = GetChannelName("SJBHChannel")
	richiestaMia = true
	SendChatMessage ("[SJBH]Get Version", "CHANNEL", nil, index)
	local messaggio = self:TestoColorato (L["nome_addon"], verdino) .. ": " .. self:TestoColorato (versioneCorrente, verdeWoW) .. " " .. self:TestoColorato (L["info_autore"], oro)
	DEFAULT_CHAT_FRAME:AddMessage (messaggio)
	if not nuovaVersione then
		messaggio = self:TestoColorato (L["ultima_versione"], verdeWoW)
		DEFAULT_CHAT_FRAME:AddMessage (messaggio)
	else
		messaggio = string.format (L["nuova_versione"], nuovaVersioneStr, "bitbucket.org/legioitalica/scholomance-jandice-barov-helper/downloads")
		DEFAULT_CHAT_FRAME:AddMessage (messaggio)
	end
end