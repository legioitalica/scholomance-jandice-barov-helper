﻿-- Author      : Galilman
-- Create Date : 21/08/2014 18:30:45

local L = LibStub("AceLocale-3.0"):NewLocale("ScholomanceJandiceBarovHelper", "itIT", true)


if L then
	L["blocca"] = "blocca"
	L["sblocca"] = "sblocca"
	L["msg_blocca"] = "Frame Bloccato"
	L["msg_sblocca"] = "Frame Sbloccato"
	L["inizializzazione"] = "Inizializzazione addon in corso"
	L["abilitato"] = "Addon abilitato e pronto all'uso"
	L["titolo_frame"] = "SJBH"
	L["mostra"] = "mostra"
	L["nascondi"] = "nascondi"
	L["frame_salvato"] = "Posizione e dimensione finestra salvati"
	L["titolo_boss"] = "Modello Boss"
	L["titolo_copia"] = "Modello Copia Corrente"
	L["nome_boss"] = "Jandice Barov"
	L["nome_copia"] = ""
	L["msg_debug"] = "Stato debug: "
	L["debug_abilitato"] = "abilitato"
	L["debug_disabilitato"] = "disabilitato"
	L["reset"] = "ripristina"
	L["resetta_posizione"] = "posizione"
	L["resetta_dimensione"] = "dimensione"
	L["collega_modelli"] = "collegamodelli"
	L["stato_modelli"] = "Stato Modelli: "
	L["stato_modelli_collegati"] = "collegati, i modelli verranno ridimensionati entrambi quando viene ridimensionato uno"
	L["stato_modelli_scollegati"] = "scollegati, i modelli verranno ridimensionati separatamente"
	L["collega"] = "collega"
	L["scollega"] = "scollega"
	L["copia_trovata"] = "La copia corretta è %s"
	L["nome_addon"] = "Scholomance Jandice Barov Helper"
	L["versione_addon_corrente"] = "Versione Corrente: %s"
	L["info_autore"] = "Creato da Legio Italica addon team"
	L["ultima_versione"] = "Stai usando l'ultima versione disponibile"
	L["nuova_versione"] = "È disponibile una nuova versione (|cff20ff20 %s |r), scaricala da |cffffd700 %s |r"
	L["informazioni_addon"] = "info"
--Messaggio aiuto
	L["help"] = [[
sintassi /sjbh [comando] {opzioni}
-[help] o nessun comando mostra questa guida
-[lock] o [blocca] blocca il frame
-[unlock] o [sblocca] sblocca il frame
-[debug]
    --nessuna opzione mostra lo stato del debug
    --{on} attiva il debug
    --{off} disattiva il debug
-[show] o [mostra] mostra il frame
-[hide] o [nascondi] nasconde il frame
-[reset] o [ripristina]
    --nessuna opzione ripristina il frame alla posizione e dimensione originale
    --{position} o {posizione} ripristina solamente la posizione
    --{size} o {dimensione} ripristina solamente la dimensione
-[linkmodel] o [collegamodelli]
    --nessuna opzione mostra lo stato dei modelli
    --{link} o {collega} collega i modelli
    --{unlink} o {scollega} scollega i modelli
-[info] informazioni addon
]]
--Fine messaggio aiuto
end