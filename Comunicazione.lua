-- Author      : Galilman
-- Create Date : 03/09/2014 20:45:32

local ScholomanceJandiceBarovHelper = _G.ScholomanceJandiceBarovHelper

local L = LibStub("AceLocale-3.0"):GetLocale("ScholomanceJandiceBarovHelper")

local risposto = false

function ScholomanceJandiceBarovHelper:CHANNEL_UI_UPDATE ()
	self:UnregisterEvent ("CHANNEL_UI_UPDATE")
	JoinTemporaryChannel ("SJBHChannel")
	self:RegisterEvent ("CHAT_MSG_CHANNEL")
end

function ScholomanceJandiceBarovHelper:CHAT_MSG_CHANNEL (type, message, sender, language, channelString, target, flags, unknown, channelNumber, channelName, unknown, counter, guid)
	if self:IsDebug () then
		self:Print ("Messaggio Arrivato: " .. message .. " spedito da " .. sender .. " in " .. channelName)
	end
	if string.lower (channelName) == "sjbhchannel" then
		--self:Print ("entro qua")
		if message == "[SJBH]Get Version" then
			if not risposto and not richiestaMia then
				local index = GetChannelName("SJBHChannel")
				SendChatMessage ("[SJBH]Version: " .. versioneCorrente, "CHANNEL", nil, index)
			end
			risposto = false
			richiestaMia = false
		elseif string.find (message, "%[SJBH%]Version:") then
			--self:Print ("entro anche qua")
			local versione = string.sub (message, 16)
			--self:Print (versione)
			if versione > versioneCorrente then
				nuovaVersione = true
				nuovaVersioneStr = versione
				messaggio = string.format (L["nuova_versione"], nuovaVersioneStr, "bitbucket.org/legioitalica/scholomance-jandice-barov-helper/downloads")
				DEFAULT_CHAT_FRAME:AddMessage (messaggio)
				risposto = true
			elseif versione < versioneCorrente then
				local index = GetChannelName("SJBHChannel")
				SendChatMessage ("[SJBH]Version: " .. versioneCorrente, "CHANNEL", nil, index)
				risposto = true
			elseif versione == versioneCorrente then
				risposto = true
			end
		end
	end 
end