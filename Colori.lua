-- Author      : Galilman
-- Create Date : 03/09/2014 18:40:00

local ScholomanceJandiceBarovHelper = _G.ScholomanceJandiceBarovHelper

function ScholomanceJandiceBarovHelper:TestoColorato (testo, colore)
	if (testo and colore) then
		local r, g, b = format("%02x", colore.r), format("%02x", colore.g), format("%02x", colore.b)
		local codiceColore = "|cff" .. r .. g .. b
		return codiceColore .. testo .. _G["FONT_COLOR_CODE_CLOSE"]
	end
end

--Colori
blu = {
	r = 0,
	g = 0,
	b = 0xff
}

verdino = {
	r = 0x33,
	g = 0xff,
	b = 0x99
}

verde = {
	r = 0,
	g = 0xff,
	b = 0
}

verdeWoW = {
	r = 0x20,
	g = 0xff,
	b = 0x20
}

battleNet = {
	r = 0x82,
	g = 0xc5,
	b = 0xff
}

oro = {
	r = 0xff,
	g = 0xd7,
	b = 0
}

giallo = {
	r = 0xff,
	g = 0xff,
	b = 0
}

gialloChiaro = {
	r = 0xff,
	g = 0xff,
	b = 0x9a
}