-- Author      : Galilman
-- Create Date : 21/08/2014 20:12:11


local ScholomanceJandiceBarovHelper = _G.ScholomanceJandiceBarovHelper

local L = LibStub("AceLocale-3.0"):GetLocale("ScholomanceJandiceBarovHelper")

local AceGUI = LibStub("AceGUI-3.0")

local MainWindow

local distanzaBossCamera = 3
local distanzaCopiaCamera = 3

--local iconaBloccato
--local iconaSbloccato
local iconaLucchettoNormale
local iconaLucchettoPremuto

function ScholomanceJandiceBarovHelper:inizializzaFrame ()
	self:creaFramePrincipale ()
	self:creaFrameModelloBoss ()
	self:creaFrameModelloCopia ()
	self:creaBarraBottoni ()

	--iconaBloccato = MainWindow.BottoneMovimento:CreateTexture(nil, "ARTWORK")
	--iconaBloccato:SetAllPoints(true)
	--iconaBloccato:SetTexture("Interface\\Buttons\\lockbutton-locked-up.blp")

	--iconaSbloccato = MainWindow.BottoneMovimento:CreateTexture(nil, "ARTWORK")
	--iconaSbloccato:SetAllPoints(true)
	--iconaSbloccato:SetTexture("Interface\\Buttons\\lockbutton-unlocked-up.blp")

	iconaLucchettoNormale = MainWindow.BottoneMovimento:CreateTexture(nil, "ARTWORK")
	iconaLucchettoNormale:SetAllPoints(true)

	iconaLucchettoPremuto = MainWindow.BottoneMovimento:CreateTexture(nil, "ARTWORK")
	iconaLucchettoPremuto:SetAllPoints(true)

	self:caricaFrame ()

	self:ridimensiona ()

	self:nascondi ()

	self:impostazioniFinali ()
end

function ScholomanceJandiceBarovHelper:mostra ()
	if MainWindow:IsShown() then
		return
	end

	MainWindow:Show ()

	if self:IsDebug () then
		local x, y, z = MainWindow.ModelloBoss.Modello:GetCameraPosition ()
		self:Print ("Posizione Camera Boss: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		x, y, z = MainWindow.ModelloBoss.Modello:GetPosition ()
		self:Print ("Posizione Modello Boss: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		self:Print ("Distanza Modello Boss Camera: " .. distanzaBossCamera)

		x, y, z = MainWindow.ModelloCopia.Modello:GetCameraPosition ()
		self:Print ("Posizione Camera Copia: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		x, y, z = MainWindow.ModelloCopia.Modello:GetPosition ()
		self:Print ("Posizione Modello Copia: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		self:Print ("Distanza Modello Copia Camera: " .. distanzaCopiaCamera)

		self:Print ("Adatto distanze ......")
	end

	self:adattaDistanze (MainWindow.ModelloBoss.Modello, distanzaBossCamera)
	self:adattaDistanze (MainWindow.ModelloCopia.Modello, distanzaCopiaCamera)

	if self:IsDebug () then
		self:Print ("Distanze adattate")
		local x, y, z = MainWindow.ModelloBoss.Modello:GetCameraPosition ()
		self:Print ("Posizione Camera Boss: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		x, y, z = MainWindow.ModelloBoss.Modello:GetPosition ()
		self:Print ("Posizione Modello Boss: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		self:Print ("Distanza Modello Boss Camera: " .. distanzaBossCamera)

		x, y, z = MainWindow.ModelloCopia.Modello:GetCameraPosition ()
		self:Print ("Posizione Camera Copia: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		x, y, z = MainWindow.ModelloCopia.Modello:GetPosition ()
		self:Print ("Posizione Modello Copia: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

		self:Print ("Distanza Modello Copia Camera: " .. distanzaCopiaCamera)
	end
end

function ScholomanceJandiceBarovHelper:nascondi ()
	self:salvaFrame ()

	if MainWindow:IsShown() then
		MainWindow:Hide ()
	end
end

function ScholomanceJandiceBarovHelper:salvaFrame ()
	--Posizione
	local point, relativeTo, relativePoint, xOfs, yOfs = MainWindow:GetPoint()
	self.db.profile.dettagliFrame.posizioneFrame.x = xOfs
	self.db.profile.dettagliFrame.posizioneFrame.y = yOfs
	self.db.profile.dettagliFrame.point = point
	self.db.profile.dettagliFrame.relativeTo = relativeTo
	self.db.profile.dettagliFrame.relativePoint = relativePoint

	--Dimensione
	self.db.profile.dettagliFrame.dimensione.x = MainWindow:GetWidth() 
	self.db.profile.dettagliFrame.dimensione.y = MainWindow:GetHeight()

	self:salvaZoom ()

	if self:IsDebug () then
		self:Print (L["frame_salvato"])
	end
end

function ScholomanceJandiceBarovHelper:salvaZoom ()
	self.db.profile.ditanzaModelloBossCamera = distanzaBossCamera
	self.db.profile.distanzaModelloCopiaCamera = distanzaCopiaCamera
end

function ScholomanceJandiceBarovHelper:caricaFrame ()
	if self.db.profile.frameBloccato then
		self:blocca ()
	else
		self:sblocca()
	end

	--Carico stato frame
	local xOfs = self.db.profile.dettagliFrame.posizioneFrame.x
	local yOfs = self.db.profile.dettagliFrame.posizioneFrame.y 
	local point = self.db.profile.dettagliFrame.point 
	local relativePoint = self.db.profile.dettagliFrame.relativePoint
	if xOfs == nil or yOfs == nil or point == nil or relativePoint == nil then
		return
	end

	MainWindow:ClearAllPoints()
	MainWindow:SetPoint(point, UIParent, relativePoint, xOfs, yOfs)

	MainWindow:SetWidth(self.db.profile.dettagliFrame.dimensione.x)
	MainWindow:SetHeight(self.db.profile.dettagliFrame.dimensione.y)

	distanzaBossCamera = self.db.profile.ditanzaModelloBossCamera
	distanzaCopiaCamera = self.db.profile.distanzaModelloCopiaCamera
end

--Carica dimensioni Frame
function ScholomanceJandiceBarovHelper:ridimensiona ()
	MainWindow.TitleTexture:SetWidth(MainWindow:GetWidth() - 15)
	
	local frame = MainWindow.ModelloBoss
	frame:SetHeight(MainWindow:GetHeight() - 100)
	frame:SetWidth(MainWindow:GetWidth() / 2)
	frame.Modello:SetWidth(frame:GetWidth () - 30)
	frame.Modello:SetHeight(frame:GetHeight () - frame.Titolo:GetHeight () - 25 - 10)

	frame = MainWindow.ModelloCopia
	frame:SetHeight(MainWindow:GetHeight() - 100)
	frame:SetWidth(MainWindow:GetWidth() / 2)
	frame.Modello:SetWidth(frame:GetWidth () - 30)
	frame.Modello:SetHeight(frame:GetHeight () - frame.Titolo:GetHeight () - 25 - 10)
end

function ScholomanceJandiceBarovHelper:blocca () 
	if self:IsDebug () then
		self:Print (L["msg_blocca"])
	end

	MainWindow.stateMove = false
	MainWindow:SetMovable (false)

	MainWindow:SetResizable(false)
	MainWindow.sizer_se:Hide ()
	MainWindow.sizer_e:Hide ()
	MainWindow.sizer_s:Hide ()

	self.db.profile.frameBloccato = true

	--MainWindow.BottoneMovimento:SetNormalTexture ("Interface\\Buttons\\lockbutton-locked.blp") 
	--MainWindow.BottoneMovimento.icon = iconaBloccato

	iconaLucchettoNormale:SetTexture ("Interface\\Buttons\\lockbutton-locked-up.blp")
	--iconaLucchettoPremuto:SetTexture ("Interface\\Buttons\\lockbutton-locked-down.blp")
	iconaLucchettoPremuto:SetTexture ("Interface\\Addons\\ScholomanceJandiceBarovHelper\\Immagini\\Bottoni\\lockbutton-locked-down.blp")
end

function ScholomanceJandiceBarovHelper:sblocca ()
	if self:IsDebug () then
		self:Print (L["msg_sblocca"])
	end

	MainWindow.stateMove = true
	MainWindow:SetMovable (true)

	MainWindow:SetResizable(true)
	MainWindow.sizer_se:Show ()
	MainWindow.sizer_s:Show ()
	MainWindow.sizer_e:Show ()

	self.db.profile.frameBloccato = false

	--MainWindow.BottoneMovimento:SetNormalTexture ("Interface\\Buttons\\lockbutton-unlocked-up.blp")
	--MainWindow.BottoneMovimento.icon = iconaSbloccato

	iconaLucchettoNormale:SetTexture ("Interface\\Buttons\\lockbutton-unlocked-up.blp")
	iconaLucchettoPremuto:SetTexture ("Interface\\Buttons\\lockbutton-unlocked-down.blp")
end

function ScholomanceJandiceBarovHelper:cambioTarget (mouseover)
	if mouseover then
		MainWindow.ModelloCopia.Modello:SetUnit ("mouseover")
	else
		MainWindow.ModelloCopia.Modello:SetUnit ("target")
	end

	self:adattaDistanze (MainWindow.ModelloCopia.Modello, distanzaCopiaCamera)
end

--Funzioni di ripristino
function ScholomanceJandiceBarovHelper:ripristina ()
	self:ripristinaPosizione ()
	self:ripristinaDimensione ()
end

function ScholomanceJandiceBarovHelper:ripristinaPosizione ()
	MainWindow:ClearAllPoints()
    MainWindow:SetPoint("CENTER",UIParent,"CENTER",0,40)
end

function ScholomanceJandiceBarovHelper:ripristinaDimensione ()
	MainWindow:SetHeight (830)
	MainWindow:SetWidth (1075)

	self:ridimensiona ()
end

--Funzioni di supporto per frame
--Mouse rilasciato su frame principale
local function MainWindow_OnMouseUp () 
	if MainWindow.stateMove then
		if MainWindow.isMoving then
			MainWindow:StopMovingOrSizing ()
			ScholomanceJandiceBarovHelper:salvaFrame ()
			MainWindow.isMoving = false
		end
	end
end

--Mouse premuto su frame principale
local function MainWindow_OnMouseDown (self, button)
	if MainWindow.stateMove then
		if button == "LeftButton" then
			MainWindow:StartMoving ()
			MainWindow.isMoving = true
		end
	end
end

--Bottone Movimento premuto
local function BottoneMovimento_Premi ()
	if MainWindow.stateMove then
		ScholomanceJandiceBarovHelper:blocca ()
	else
		ScholomanceJandiceBarovHelper:sblocca ()
    end
end

--Bottone Raid Mark premuto
local function BottoneMark_Premi (self)
	if self:GetName() == "SJBH_BottoneTeschio" then
		ScholomanceJandiceBarovHelper:setMark (8)
	elseif self:GetName() == "SJBH_BottoneCroce" then
		ScholomanceJandiceBarovHelper:setMark (7)
	elseif self:GetName() == "SJBH_BottoneQuadrato" then
		ScholomanceJandiceBarovHelper:setMark (6)
	elseif self:GetName() == "SJBH_BottoneLuna" then
		ScholomanceJandiceBarovHelper:setMark (5)
	elseif self:GetName() == "SJBH_BottoneTriangolo" then
		ScholomanceJandiceBarovHelper:setMark (4)
	elseif self:GetName() == "SJBH_BottoneRombo" then
		ScholomanceJandiceBarovHelper:setMark (3)
	elseif self:GetName() == "SJBH_BottoneCerchio" then
		ScholomanceJandiceBarovHelper:setMark (2)
	elseif self:GetName() == "SJBH_BottoneStella" then
		ScholomanceJandiceBarovHelper:setMark (1)
	end
end

--Eventi ridimensionamento
local function FineRidimensiona ()
	MainWindow:StopMovingOrSizing ()

	ScholomanceJandiceBarovHelper:salvaFrame ()
end

local function SizerSE_OnMouseDown(frame)
	frame:GetParent():StartSizing("BOTTOMRIGHT")
end

local function SizerS_OnMouseDown(frame)
	frame:GetParent():StartSizing("BOTTOM")
end

local function SizerE_OnMouseDown(frame)
	frame:GetParent():StartSizing("RIGHT")
end

local function MainWindow_OnSizeChanged (width, height)
	ScholomanceJandiceBarovHelper:ridimensiona ()
end

--Ridimensiona modello
local function Model_OnMouseWheel(self, delta)
	if ScholomanceJandiceBarovHelper:IsModelliCollegati () then
		local xBoss, yBoss, zBoss = MainWindow.ModelloBoss.Modello:GetPosition()
		local scale = (IsControlKeyDown() and 2 or 0.7)

		xBoss = (delta > 0 and xBoss + scale or xBoss - scale)

		MainWindow.ModelloBoss.Modello:SetPosition(xBoss, yBoss, zBoss)

		distanzaBossCamera = ScholomanceJandiceBarovHelper:distanzaModelloCamera (MainWindow.ModelloBoss.Modello)

		local xCopia, yCopia, zCopia = MainWindow.ModelloCopia.Modello:GetPosition()

		xCopia = (delta > 0 and xCopia + scale or xCopia - scale)

		MainWindow.ModelloCopia.Modello:SetPosition(xCopia, yCopia, zCopia)

		distanzaCopiaCamera = ScholomanceJandiceBarovHelper:distanzaModelloCamera (MainWindow.ModelloCopia.Modello)

		if ScholomanceJandiceBarovHelper:IsDebug () then
			local x, y, z = MainWindow.ModelloBoss.Modello:GetCameraPosition ()
			ScholomanceJandiceBarovHelper:Print ("Posizione Camera Boss: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

			x, y, z = MainWindow.ModelloBoss.Modello:GetPosition ()
			ScholomanceJandiceBarovHelper:Print ("Posizione Modello Boss: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

			ScholomanceJandiceBarovHelper:Print ("Distanza Modello Boss Camera: " .. distanzaBossCamera)

			x, y, z = MainWindow.ModelloCopia.Modello:GetCameraPosition ()
			ScholomanceJandiceBarovHelper:Print ("Posizione Camera Copia: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

			x, y, z = MainWindow.ModelloCopia.Modello:GetPosition ()
			ScholomanceJandiceBarovHelper:Print ("Posizione Modello Copia: x = " .. (x and x or "bho!") .. " y = " .. (y and y or "bho!") .. " z = " .. (z and z or "bho!"))

			ScholomanceJandiceBarovHelper:Print ("Distanza Modello Copia Camera: " .. distanzaCopiaCamera)
		end
	else
		local x, y, z = self:GetPosition()
		local scale = (IsControlKeyDown() and 2 or 0.7)
		x = (delta > 0 and x + scale or x - scale)

		self:SetPosition(x, y, z);

		if self:GetName () == "SJBH_ModelloBoss" then
			distanzaBossCamera = ScholomanceJandiceBarovHelper:distanzaModelloCamera (MainWindow.ModelloBoss.Modello)
		elseif self:GetName () == "SJBH_ModelloCopia" then
			distanzaCopiaCamera = ScholomanceJandiceBarovHelper:distanzaModelloCamera (MainWindow.ModelloCopia.Modello)
		end
	end
end

--Backdrop frame
local MainWindow_Backdrop = {
	bgFile = "Interface\\Tooltips\\UI-Tooltip-Background", tile = true, tileSize = 12,
    edgeFile = "Interface/Tooltips/UI-Tooltip-Border", edgeSize = 32,
    insets = {left = 7, right = 7, top = 7, bottom = 7},
}

--Crea il frame principale
function ScholomanceJandiceBarovHelper:creaFramePrincipale ()
	if self:IsDebug () then
		self:Print ("Creo Frame")
	end

	local screenX = GetScreenWidth() - 140
    local screenY = GetScreenHeight() - 130

    local mainX = 1075
    local mainY = 830

    local scale = 1.0

    if(screenY < mainY)then
        scale = screenY/mainY
    elseif(screenX < mainX)then
        scale = screenX/mainX
    end

	MainWindow = CreateFrame("Frame", "SJBH_MainFrame",UIParent)

    MainWindow.stateMove = false
    MainWindow:ClearAllPoints()
    MainWindow:SetPoint("CENTER",UIParent,"CENTER",0,40)
    MainWindow:SetHeight(mainY)
    MainWindow:SetFrameLevel(100)
    MainWindow:SetToplevel(true)
    MainWindow:SetWidth(mainX)
    MainWindow:SetScale(scale)
	MainWindow:SetMinResize(550, 460)

	--Barra Blu Sopra
    MainWindow.TitleTexture = MainWindow:CreateTexture("SJBH_TitleBG", "BACKGROUND")
    MainWindow.TitleTexture:SetPoint("TOPLEFT", MainWindow, "TOPLEFT", 7, -5)
    MainWindow.TitleTexture:SetWidth(MainWindow:GetWidth() - 15)
    MainWindow.TitleTexture:SetHeight(35)
    MainWindow.TitleTexture:SetTexture(16/255, 24/255, 177/255)

	--Aspetto
	MainWindow:SetBackdrop(MainWindow_Backdrop)
    MainWindow:SetBackdropBorderColor(0,0,1.0)
    MainWindow:SetBackdropColor(24/255, 24/255, 24/255)
    MainWindow:EnableMouse(true)
    MainWindow:SetMovable(false)

	--Titolo
	local titlebg = MainWindow:CreateTexture(nil, "OVERLAY")
	titlebg:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
	titlebg:SetTexCoord(0.31, 0.67, 0, 0.63)
	titlebg:SetPoint("TOP", 0, 12)
	titlebg:SetWidth(100)
	titlebg:SetHeight(40)

	local title = CreateFrame("Frame", nil, MainWindow)
	title:EnableMouse(true)
	title:SetScript("OnMouseDown", MainWindow_OnMouseDown)
	title:SetScript("OnMouseUp", MainWindow_OnMouseUp)
	title:SetAllPoints(titlebg)

	local titletext = MainWindow:CreateFontString(nil, "OVERLAY", "GameFontNormal")
	titletext:SetPoint("TOP", titlebg, "TOP", 0, -14)

	local titlebg_l = MainWindow:CreateTexture(nil, "OVERLAY")
	titlebg_l:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
	titlebg_l:SetTexCoord(0.21, 0.31, 0, 0.63)
	titlebg_l:SetPoint("RIGHT", titlebg, "LEFT")
	titlebg_l:SetWidth(30)
	titlebg_l:SetHeight(40)

	local titlebg_r = MainWindow:CreateTexture(nil, "OVERLAY")
	titlebg_r:SetTexture("Interface\\DialogFrame\\UI-DialogBox-Header")
	titlebg_r:SetTexCoord(0.67, 0.77, 0, 0.63)
	titlebg_r:SetPoint("LEFT", titlebg, "RIGHT")
	titlebg_r:SetWidth(30)
	titlebg_r:SetHeight(40)

	MainWindow.Titolo = titletext
    MainWindow.Titolo:SetText(L["titolo_frame"])

	--Bottone Movimento
	MainWindow.BottoneMovimento = CreateFrame("Button", "SJBH_movemainframe", MainWindow)
    --MainWindow.BottoneMovimento:SetNormalTexture("Interface\\Buttons\\UI-AttributeButton-Encourage-Up.blp")
    --MainWindow.BottoneMovimento:SetPushedTexture("Interface\\Buttons\\UI-AttributeButton-Encourage-Down.blp")
    --MainWindow.BottoneMovimento:SetHighlightTexture("Interface\\Buttons\\UI-AttributeButton-Encourage-Hilight.blp")
    MainWindow.BottoneMovimento:SetWidth(28)
    MainWindow.BottoneMovimento:SetHeight(28)
    MainWindow.BottoneMovimento:SetPoint("TOPRIGHT", MainWindow, "TOPRIGHT", -30, -11)
    MainWindow.BottoneMovimento:SetScript("OnClick", BottoneMovimento_Premi)

	--Ridimensiona
	local sizer_se = CreateFrame("Frame", nil, MainWindow)
	sizer_se:SetPoint("BOTTOMRIGHT", -2, 5)
	sizer_se:SetWidth(25)
	sizer_se:SetHeight(25)
	sizer_se:EnableMouse()
	sizer_se:SetScript("OnMouseDown",SizerSE_OnMouseDown)
	sizer_se:SetScript("OnMouseUp", FineRidimensiona)

	local line1 = sizer_se:CreateTexture(nil, "BACKGROUND")
	line1:SetWidth(14)
	line1:SetHeight(14)
	line1:SetPoint("BOTTOMRIGHT", -8, 8)
	line1:SetTexture("Interface\\Tooltips\\UI-Tooltip-Border")
	local x = 0.1 * 14/17
	line1:SetTexCoord(0.05 - x, 0.5, 0.05, 0.5 + x, 0.05, 0.5 - x, 0.5 + x, 0.5)

	local line2 = sizer_se:CreateTexture(nil, "BACKGROUND")
	line2:SetWidth(8)
	line2:SetHeight(8)
	line2:SetPoint("BOTTOMRIGHT", -8, 8)
	line2:SetTexture("Interface\\Tooltips\\UI-Tooltip-Border")
	local x = 0.1 * 8/17
	line2:SetTexCoord(0.05 - x, 0.5, 0.05, 0.5 + x, 0.05, 0.5 - x, 0.5 + x, 0.5)

	local sizer_s = CreateFrame("Frame", nil, MainWindow)
	sizer_s:SetPoint("BOTTOMRIGHT", -25, 0)
	sizer_s:SetPoint("BOTTOMLEFT")
	sizer_s:SetHeight(25)
	sizer_s:EnableMouse(true)
	sizer_s:SetScript("OnMouseDown", SizerS_OnMouseDown)
	sizer_s:SetScript("OnMouseUp", FineRidimensiona)

	local sizer_e = CreateFrame("Frame", nil, MainWindow)
	sizer_e:SetPoint("BOTTOMRIGHT", 0, 25)
	sizer_e:SetPoint("TOPRIGHT")
	sizer_e:SetWidth(25)
	sizer_e:EnableMouse(true)
	sizer_e:SetScript("OnMouseDown", SizerE_OnMouseDown)
	sizer_e:SetScript("OnMouseUp", FineRidimensiona)

	MainWindow.sizer_se = sizer_se
	MainWindow.sizer_s = sizer_s
	MainWindow.sizer_e = sizer_e
end

--Crea frame modello boss
function ScholomanceJandiceBarovHelper:creaFrameModelloBoss ()
	if MainWindow ~= nil then
		--Contenitore
		MainWindow.ModelloBoss = CreateFrame("Frame", "SJBH_LeftPane", MainWindow)
        MainWindow.ModelloBoss:ClearAllPoints ()
        MainWindow.ModelloBoss:SetPoint("TOPLEFT", MainWindow, "TOPLEFT", 0, -40)
        MainWindow.ModelloBoss:SetHeight(MainWindow:GetHeight() - 110)
        MainWindow.ModelloBoss:SetWidth(MainWindow:GetWidth() / 2)
		MainWindow.ModelloBoss:SetResizable(true)

		local frame = MainWindow.ModelloBoss
		frame.Titolo = frame:CreateFontString(nil, "OVERLAY", "MailTextFontNormal")
        frame.Titolo:SetTextColor(226/255 ,186/255, 0)
        frame.Titolo:SetText(L["titolo_boss"])
        frame.Titolo:SetPoint("TOP", frame, "Top", 0, -15)

		frame.Modello = CreateFrame("PlayerModel", "SJBH_ModelloBoss", MainWindow.ModelloBoss)
		frame.Modello:SetWidth(frame:GetWidth () - 30)
        frame.Modello:SetHeight(frame:GetHeight () - frame.Titolo:GetHeight () - 25 - 10)
		frame.Modello:SetPoint("TOPLEFT", frame, "TOPLEFT", 15, - (frame.Titolo:GetHeight () + 20))
		frame.Modello:SetBackdrop(MainWindow_Backdrop)
		frame.Modello:SetResizable(true)		

		--Ridimensionamento e spostamento modello
		frame.Modello:EnableMouse(true);
		frame.Modello:EnableMouseWheel(true);
		frame.Modello:SetScript("OnMouseWheel", Model_OnMouseWheel);

		frame.Modello:SetCreature (Jandice.Originale["id"])

		distanzaBossCamera = self:distanzaModelloCamera (frame.Modello)
	end
end

--Crea frame modello copia
function ScholomanceJandiceBarovHelper:creaFrameModelloCopia ()
	if MainWindow ~= nil then
		--Contenitore
		MainWindow.ModelloCopia = CreateFrame("Frame", "SJBH_RightPane", MainWindow)
        MainWindow.ModelloCopia:ClearAllPoints ()
        MainWindow.ModelloCopia:SetPoint("TOPRIGHT", MainWindow, "TOPRIGHT", 0, -40)
        MainWindow.ModelloCopia:SetHeight(MainWindow:GetHeight() - 110)
        MainWindow.ModelloCopia:SetWidth(MainWindow:GetWidth() / 2)
		MainWindow.ModelloCopia:SetResizable(true)

		local frame = MainWindow.ModelloCopia
		frame.Titolo = frame:CreateFontString(nil, "OVERLAY", "MailTextFontNormal")
        frame.Titolo:SetTextColor(226/255 ,186/255, 0)
        frame.Titolo:SetText(L["titolo_copia"])
        frame.Titolo:SetPoint("TOP", frame, "Top", 0, -15)

		frame.Modello = CreateFrame("PlayerModel", "SJBH_ModelloCopia", MainWindow.ModelloCopia)
		frame.Modello:SetWidth(frame:GetWidth () - 30)
        frame.Modello:SetHeight(frame:GetHeight () - frame.Titolo:GetHeight () - 25 - 10)
		frame.Modello:SetPoint("TOPLEFT", frame, "TOPLEFT", 15, - (frame.Titolo:GetHeight () + 20))
		frame.Modello:SetBackdrop(MainWindow_Backdrop)
		frame.Modello:SetResizable(true)

		frame.Modello:EnableMouse(true);
		frame.Modello:EnableMouseWheel(true);
		frame.Modello:SetScript("OnMouseWheel", Model_OnMouseWheel);

		frame.Modello:SetCreature (Jandice.Originale["id"])

		distanzaCopiaCamera = self:distanzaModelloCamera (frame.Modello)
	end
end

--Crea la barra con i bottoni per i mark
function ScholomanceJandiceBarovHelper:creaBarraBottoni ()
	if MainWindow ~= nil then
		MainWindow.BarraBottoni = CreateFrame ("Frame", "SJBH_BottomPane", MainWindow)
		
		local frame = MainWindow.BarraBottoni
		frame:ClearAllPoints ()
		frame:SetPoint("BOTTOMLEFT", MainWindow, "BOTTOMLEFT", 0, 5)
		frame:SetPoint ("BOTTOMRIGHT", MainWindow, "BOTTOMRIGHT", 0, 5)
		frame:SetHeight (40)

		MainWindow.BottomTitleTexture = MainWindow:CreateTexture("SJBH_BottomTitleBG", "BACKGROUND")
		MainWindow.BottomTitleTexture:SetPoint("BOTTOMLEFT", MainWindow, "BOTTOMLEFT", 7, 5)
		MainWindow.BottomTitleTexture:SetPoint("BOTTOMRIGHT", MainWindow, "BOTTOMRIGHT", -7, 5)
		--frame.TitleTexture:SetWidth(MainWindow:GetWidth() - 15)
		MainWindow.BottomTitleTexture:SetHeight(40)
		MainWindow.BottomTitleTexture:SetTexture(16/255, 24/255, 177/255)

		--Bottone Teschio Bianco
		frame.BottoneTeschio = CreateFrame ("Button", "SJBH_BottoneTeschio", frame)
		frame.BottoneTeschio:SetWidth(25)
		frame.BottoneTeschio:SetHeight(25)
		frame.BottoneTeschio:SetPoint ("TOPLEFT", frame, "TOPLEFT", 15, -3)
		frame.BottoneTeschio:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		local icona = frame.BottoneTeschio:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_8")
		frame.BottoneTeschio.icon = icona
		
		--Bottone Croce Rossa
		frame.BottoneCroce = CreateFrame ("Button", "SJBH_BottoneCroce", frame)
		frame.BottoneCroce:SetWidth(25)
		frame.BottoneCroce:SetHeight(25)
		frame.BottoneCroce:SetPoint ("TOPLEFT", frame.BottoneTeschio, "TOPRIGHT", 15, 0)
		frame.BottoneCroce:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneCroce:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_7")
		frame.BottoneCroce.icon = icona

		--Bottone Stella Gialla
		frame.BottoneStella = CreateFrame ("Button", "SJBH_BottoneStella", frame)
		frame.BottoneStella:SetWidth(25)
		frame.BottoneStella:SetHeight(25)
		frame.BottoneStella:SetPoint ("TOPLEFT", frame.BottoneCroce, "TOPRIGHT", 15, 0)
		frame.BottoneStella:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneStella:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_1")
		frame.BottoneStella.icon = icona

		--Bottone Palla Arancione
		frame.BottoneCerchio = CreateFrame ("Button", "SJBH_BottoneCerchio", frame)
		frame.BottoneCerchio:SetWidth(25)
		frame.BottoneCerchio:SetHeight(25)
		frame.BottoneCerchio:SetPoint ("TOPLEFT", frame.BottoneStella, "TOPRIGHT", 15, 0)
		frame.BottoneCerchio:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneCerchio:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_2")
		frame.BottoneCerchio.icon = icona

		--Bottone Rombo Viola
		frame.BottoneRombo = CreateFrame ("Button", "SJBH_BottoneRombo", frame)
		frame.BottoneRombo:SetWidth(25)
		frame.BottoneRombo:SetHeight(25)
		frame.BottoneRombo:SetPoint ("TOPLEFT", frame.BottoneCerchio, "TOPRIGHT", 15, 0)
		frame.BottoneRombo:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneRombo:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_3")
		frame.BottoneRombo.icon = icona

		--Bottone Triangolo Verde
		frame.BottoneTriangolo = CreateFrame ("Button", "SJBH_BottoneTriangolo", frame)
		frame.BottoneTriangolo:SetWidth(25)
		frame.BottoneTriangolo:SetHeight(25)
		frame.BottoneTriangolo:SetPoint ("TOPLEFT", frame.BottoneRombo, "TOPRIGHT", 15, 0)
		frame.BottoneTriangolo:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneTriangolo:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_4")
		frame.BottoneTriangolo.icon = icona

		--Bottone Luna Argento
		frame.BottoneLuna = CreateFrame ("Button", "SJBH_BottoneLuna", frame)
		frame.BottoneLuna:SetWidth(25)
		frame.BottoneLuna:SetHeight(25)
		frame.BottoneLuna:SetPoint ("TOPLEFT", frame.BottoneTriangolo, "TOPRIGHT", 15, 0)
		frame.BottoneLuna:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneLuna:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_5")
		frame.BottoneLuna.icon = icona

		--Bottone Quadrato Blu
		frame.BottoneQuadrato = CreateFrame ("Button", "SJBH_BottoneQuadrato", frame)
		frame.BottoneQuadrato:SetWidth(25)
		frame.BottoneQuadrato:SetHeight(25)
		frame.BottoneQuadrato:SetPoint ("TOPLEFT", frame.BottoneLuna, "TOPRIGHT", 15, 0)
		frame.BottoneQuadrato:SetScript ("OnClick", BottoneMark_Premi)
		--Icona
		icona = frame.BottoneQuadrato:CreateTexture(nil, "ARTWORK")
		icona:SetAllPoints(true)
		icona:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_6")
		frame.BottoneQuadrato.icon = icona
	end 
end

--Imposto script e altri elementi che devono essere aggiunti dopo l'inizializzazione
function ScholomanceJandiceBarovHelper:impostazioniFinali ()
	MainWindow:SetScript ("OnSizeChanged", MainWindow_OnSizeChanged)

	--Icona lucchetto
	MainWindow.BottoneMovimento:SetNormalTexture (iconaLucchettoNormale)
	MainWindow.BottoneMovimento:SetPushedTexture (iconaLucchettoPremuto)
end

function ScholomanceJandiceBarovHelper:distanzaModelloCamera (playerModel)
	local xMod, yMod, zMod = playerModel:GetPosition () or 0, 0, 0
	local xCam, yCam, zCam = playerModel:GetCameraPosition () or 0, 0, 0

	local distanzaX = xCam - xMod

	return distanzaX
end

function ScholomanceJandiceBarovHelper:adattaDistanze (playerModel, distanza)
	local adatta = self:distanzaModelloCamera (playerModel) - distanza
	local x, y, z = playerModel:GetPosition ()

	x = x + adatta

	playerModel:SetPosition (x, y, z)
end