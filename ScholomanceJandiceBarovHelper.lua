ScholomanceJandiceBarovHelper = LibStub("AceAddon-3.0"):NewAddon("ScholomanceJandiceBarovHelper", "AceConsole-3.0", "AceEvent-3.0" );

--Localizzazione
local L = LibStub("AceLocale-3.0"):GetLocale("ScholomanceJandiceBarovHelper")

--Valori default
local defaults = {
	profile = {
		frameBloccato = false,
		debugAttivo = false,
		modelliCollegati = true,
		dettagliFrame = {
			posizioneFrame = {
				x = 998877665544,
				y = 998877665544
			},
			dimensione = {
				x = 500,
				y = 500
			},
		},
		distanzaModelloBossCamera = 3,
		distanzaModelloCopiaCamera = 3
	}
}

--Dettagli mob
Jandice = {
	Originale = {
		["nome"] = L["nome_boss"],
		["id"] = 59184,
	},
	Copia = {
		["nome"] = L["nome_copia"],
		["id"] = 59220,
	},
}

--Opzioni
--local opzioni = {
	--name = "Scholomance Jandice Barov Helper",
	--handler = ScholomanceJandiceBarovHelper,
	--type = 'group',
    --args = {
		--Opzioni
    --},
--}

function ScholomanceJandiceBarovHelper:OnInitialize()
	-- Called when the addon is loaded

	-- Carico le variabili salvate
	self.db = LibStub("AceDB-3.0"):New("SJBHDB", defaults, true)

	if self:IsDebug () then
		self:Print(L["inizializzazione"])
	end

	self:stampaInfo()

	--Inizializzo opzioni
	--LibStub("AceConfig-3.0"):RegisterOptionsTable("ScholomanceJandiceBarovHelper", opzioni)
	--self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Scholomance Jandice Barov Helper", "ScholomanceJandiceBarovHelper")

	-- Inizializzo comandi chat
	self:RegisterChatCommand ("sjbh", "ChatCommand")
	self:inizializzaFrame ()
end

function ScholomanceJandiceBarovHelper:OnEnable()
	-- Called when the addon is enabled

	-- Print a message to the chat frame
	self:Print(L["abilitato"])

	-- Registro gli eventi
	self:RegisterEvent ("PLAYER_TARGET_CHANGED")
	self:RegisterEvent ("UPDATE_MOUSEOVER_UNIT")
	self:RegisterEvent ("PLAYER_LOGOUT")

	-- Controllo se i canali sono gi� stati inizializzati
	if GetNumDisplayChannels() > 0 then 
		-- Se si aggiungo il mio
		self:CHANNEL_UI_UPDATE()
	else
		self:RegisterEvent ("CHANNEL_UI_UPDATE")
	end
end

function ScholomanceJandiceBarovHelper:OnDisable()
	-- Called when the addon is disabled
	self:salvaFrame ()
end


function ScholomanceJandiceBarovHelper:ChatCommand(input)
  -- Process the slash command ('input' contains whatever follows the slash command)
	if not input or input:trim() == "" or input == "help" then
        DEFAULT_CHAT_FRAME:AddMessage (self:TestoColorato (L["help"], giallo))
	elseif input == "lock" or input == L["blocca"] then
		self:blocca()
	elseif input == "unlock" or input == L["sblocca"] then
		self:sblocca()
	elseif input == "show" or input == L["mostra"] then
		self:mostra()
	elseif input == "hide" or input == L["nascondi"] then
		self:nascondi ()
	elseif string.find (input, "debug") then
		local split = self:split (input)
		if table.getn(split) == 1 then
			self:Print (L["msg_debug"] .. (self:IsDebug() and L["debug_abilitato"] or L["debug_disabilitato"]))
		elseif split[2] == "on" then
			self:SetDebug (true)
		elseif split[2] == "off" then
			self:SetDebug (false)
		end
	elseif string.find (input, "reset") or string.find (input, L["reset"]) then
		local split = self:split (input)
		if table.getn(split) == 1 then
			self:ripristina ()
		elseif split[2] == "position" or split[2] == L["resetta_posizione"] then
			self:ripristinaPosizione ()
		elseif split[2] == "size" or split[2] == L["resetta_dimensione"] then
			self:ripristinaDimensione ()
		end
	elseif string.find (input, "linkmodel") or string.find (input, L["collega_modelli"]) then
		local split = self:split (input)
		if table.getn(split) == 1 then
			self:Print (L["stato_modelli"] .. (self:IsModelliCollegati () and L["stato_modelli_collegati"] or L["stato_modelli_scollegati"]))
		elseif split[2] == "link" or split[2] == L["collega"] then
			self:SetModelliCollegati (true)
		elseif split[2] == "unlink" or split[2] == L["scollega"] then
			self:SetModelliCollegati (false)
		end
	elseif input == "info" or input == L["informazioni_addon"] then
		self:stampaInfo()
	--elseif input == "settings" then
		--InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
	else
		DEFAULT_CHAT_FRAME:AddMessage (self:TestoColorato (L["help"], giallo))
	end
end

--Eventi
function ScholomanceJandiceBarovHelper:PLAYER_TARGET_CHANGED()
	mobData = self:targetInfo ()
	if (self:IsDebug ()) then
		self:Print ("Cambiato Target: " .. mobData["nome"] .. " Guid: " .. mobData["GUID"] .. " id " .. mobData["id"] .. " stato " .. mobData["stato"])
	end

	if mobData["tipo"] == "NPC" then
		if mobData["id"] == Jandice.Originale["id"] and mobData["stato"] == "vivo" then
			if self:IsDebug () then
				self:Print ("Targettato Jandice Originale")
			end
			self:mostra()
			self:RegisterEvent ("PLAYER_REGEN_ENABLED")
		elseif mobData["id"] == Jandice.Copia["id"] then
			if self:IsDebug () then
				self:Print ("Targettata copia")
			end
			self:cambioTarget()
		end
	end
end

function ScholomanceJandiceBarovHelper:PLAYER_REGEN_ENABLED()
	self:nascondi ()
	self:UnregisterEvent ("PLAYER_REGEN_ENABLED")
end

function ScholomanceJandiceBarovHelper:UPDATE_MOUSEOVER_UNIT ()
	mobData = self:mouseoverInfo ()
	if (self:IsDebug ()) then
		self:Print ("Mouse su: " .. mobData["nome"] .. " Guid: " .. mobData["GUID"] .. " id " .. mobData["id"] .. " stato " .. mobData["stato"])
	end

	if mobData["tipo"] == "NPC" then
		if mobData["id"] == Jandice.Originale["id"] and mobData["stato"] == "vivo" then
			if self:IsDebug () then
				self:Print ("Mouse su Jandice Originale")
			end
			self:mostra()
			self:RegisterEvent ("PLAYER_REGEN_ENABLED")
		elseif mobData["id"] == Jandice.Copia["id"] then
			if self:IsDebug () then
				self:Print ("Mouse su copia")
			end
			self:cambioTarget(true)
		end
	end
end

function ScholomanceJandiceBarovHelper:PLAYER_LOGOUT ()
	LeaveChannelByName ("SJBHChannel")
end